package cse190.classchat;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.Random;

public class PostForumActivity extends Activity {

    private static final String FIREBASE_URL = "https://cse190classchat.firebaseio.com/Forum";

    private String mUsername;
    private Firebase mFirebaseRef;
    //private ValueEventListener mConnectedListener;
    private ForumListAdapter mForumListAdapter;

    //private String objectType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_forum);

        Firebase.setAndroidContext(this);

        Intent intent_objectId = getIntent();
        //objectType = intent_objectId.getExtras().getString("objectType");

        //Toast.makeText(ChatdisplayActivity.this,
        //        "here", Toast.LENGTH_LONG).show();

        // Make sure we have a mUsername
        //setupUsername();

        //setTitle("Chatting as " + mUsername);


        mFirebaseRef = new Firebase(FIREBASE_URL).child("Lecture");

        // Setup our input methods. Enter key on the keyboard or pushing the send button
        EditText topic = (EditText) findViewById(R.id.topicInput);
        EditText detail = (EditText) findViewById(R.id.detailInput);
        /*
        topic.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });

        detail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });
         */
        findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        /*
        // Finally, a little indication of connection status
        mConnectedListener = mFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    Toast.makeText(PostForumActivity.this, "Connected to Firebase", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PostForumActivity.this, "Disconnected from Firebase", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // No-op
            }
        });
        */
    }

    @Override
    public void onStop() {
        super.onStop();
       /* mFirebaseRef.getRoot().child(".info/connected").removeEventListener(mConnectedListener);
        mForumListAdapter.cleanup(); */
    }

    private void setupUsername() {
        SharedPreferences prefs = getApplication().getSharedPreferences("ChatPrefs", 0);
        mUsername = prefs.getString("username", null);
        if (mUsername == null) {
            mUsername = "Yoonho";
            prefs.edit().putString("username", mUsername).commit();
        }
    }

    private void sendMessage() {
        EditText editTopic = (EditText) findViewById(R.id.topicInput);
        EditText editDetail = (EditText) findViewById(R.id.detailInput);
        String stringTopic = editTopic.getText().toString();
        String stringDetail = editDetail.getText().toString();
        if (!stringTopic.equals("")) {
            // Create our 'model', a Chat object
            Forum forum = new Forum(stringTopic, stringDetail);
            // Create a new, auto-generated child of that chat location, and save our chat data there
            mFirebaseRef.push().setValue(forum);
            editTopic.setText("");
            editDetail.setText("");
            Toast.makeText(PostForumActivity.this, "Forum has been made by " + mUsername, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(PostForumActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
