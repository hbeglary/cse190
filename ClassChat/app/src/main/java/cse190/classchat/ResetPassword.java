package cse190.classchat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;


public class ResetPassword extends AppCompatActivity {

    private static final String FIREBASE_URL = "https://cse190classchat.firebaseio.com/";
    private AlertDialog.Builder alertDialog;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);
        Firebase.setAndroidContext(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reset_password, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SendEmail(View view){
        Firebase ref = new Firebase(FIREBASE_URL);
        String email = ((EditText) findViewById(R.id.email_input)).getText().toString();
        ref.resetPassword(email, new Firebase.ResultHandler(){
            @Override
            public void onSuccess(){
                Alert("Password reset email sent successfully");
                finish();
            }
            @Override
            public void onError(FirebaseError error){
                Alert("Erro sending password reset email");
            }
        });
    }

    public void Alert(String error){
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(error);

        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = alertDialog.create();
        alert11.show();
    }
}
