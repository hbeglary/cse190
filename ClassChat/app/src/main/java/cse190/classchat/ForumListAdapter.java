package cse190.classchat;

import android.app.Activity;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.firebase.client.Query;

public class ForumListAdapter extends FirebaseListAdapter<Forum> {

    //private String mUsername;

    public ForumListAdapter(Query ref, Activity activity, int layout, String mUsername) {
        super(ref, Forum.class, layout, activity);
        //this.mUsername = mUsername;
    }

    @Override
    protected void populateView(View view, Forum forum) {

        TextView TopicText = (TextView) view.findViewById(R.id.topic);
        TextView DetailText = (TextView) view.findViewById(R.id.detail);

        TopicText.setText("Topic" + ": ");
        DetailText.setText("Detail" + ": ");

        TopicText.setTextColor(Color.BLUE);
        DetailText.setTextColor(Color.BLACK);

        ((TextView) view.findViewById(R.id.topic)).setText(forum.getTopic());
        ((TextView) view.findViewById(R.id.detail)).setText(forum.getDetail());


    }
}
