package cse190.classchat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Map;

public class Register extends AppCompatActivity {

    private static final String FIREBASE_URL = "https://cse190classchat.firebaseio.com/";
    private EditText email = null;
    private EditText password = null;
    private EditText repeat = null;
    private AlertDialog.Builder alertDialog;
    final Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        Firebase.setAndroidContext(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SubmitClick(View view){

        email = (EditText) findViewById(R.id.email_value);
        password = (EditText) findViewById(R.id.password_input);
        repeat = (EditText) findViewById(R.id.repeat_input);

        String mail = email.getText().toString();
        String pass = password.getText().toString();
        String rep = repeat.getText().toString();

        if(!isValidEmailAddress(mail)){

            if(!pass.equals(rep)){
                Alert("Incorrect Email!\nPassword Do Not Match!");
            }else{
                Alert("Incorrect Email!");
            }
        }
        else if(!pass.equals(rep)){
            Alert("Passwords Do Not Match!");
        }
        else{
            final Firebase firebase = new Firebase(FIREBASE_URL);
            firebase.createUser(mail, pass, new Firebase.ValueResultHandler<Map<String, Object>>() {
                @Override
                public void onSuccess(Map<String, Object> result) {
                    Alert("Registered Account");
                    finish();

                }
                @Override
                public void onError(FirebaseError firebaseError) {
                    Alert(firebaseError.getMessage());
                }
            });

        }
    }

    public void CancelClick(View view){
        finish();
    }

    public void Alert(String error){
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(error);

        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = alertDialog.create();
        alert11.show();
    }


    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
