package cse190.classchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View.OnClickListener;

public class ChatFragment extends Fragment {
    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(ChatActivity.this, ChatdisplayActivity.class);
        intent.putExtra("objectId", "this is object Id");
        startActivity(intent);
    }
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat,container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button publicChat = (Button) getActivity().findViewById(R.id.sendButton1);
        publicChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChatdisplayActivity.class);
                intent.putExtra("objectType", "public");
                getActivity().startActivity(intent);
            }
        });

        Button privateChat = (Button) getActivity().findViewById(R.id.sendButton2);
        privateChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChatdisplayActivity.class);
                intent.putExtra("objectType", "private");
                getActivity().startActivity(intent);
            }
        });

        Button forumChat = (Button) getActivity().findViewById(R.id.sendButton3);
        forumChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChatdisplayActivity.class);
                intent.putExtra("objectType", "forum");
                getActivity().startActivity(intent);
            }
        });
    }
}
