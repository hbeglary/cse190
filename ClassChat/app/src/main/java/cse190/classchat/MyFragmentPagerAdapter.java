package cse190.classchat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter{

    final int PAGE_COUNT = 3;

    /** Constructor of the class */
    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /** This method will be invoked when a page is requested to create */
    @Override
    public Fragment getItem(int arg0) {
        Bundle data = new Bundle();
        switch(arg0){

            /** tab1 is selected */
            case 0:
                ForumFragment forumFragment = new ForumFragment();
                return forumFragment;

            /** tab2 is selected */
            case 1:
                ClickerFragment clickerFragment = new ClickerFragment();
                return clickerFragment;

            /** tab3 is selected */
            case 2:
                PodcastFragment podcastFragment = new PodcastFragment();
                return podcastFragment;
        }
        return null;
    }

    /** Returns the number of pages */
    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
