package cse190.classchat;

public class Forum {

    private String topic;
    private String detail;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Forum() {
    }

    Forum(String topic, String detail) {
        this.topic = topic;
        this.detail = detail;
    }

    public String getTopic() {
        return topic;
    }

    public String getDetail() {
        return detail;
    }
}
