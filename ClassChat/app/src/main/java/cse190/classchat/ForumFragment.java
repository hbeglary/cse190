package cse190.classchat;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class ForumFragment extends ListFragment {

    private static final String FIREBASE_URL = "https://cse190classchat.firebaseio.com/Forum";

    private String mUsername;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    private ForumListAdapter mForumListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Firebase.setAndroidContext(getActivity());

        mFirebaseRef = new Firebase(FIREBASE_URL).child("Lecture");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forum, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        // Make sure we have a mUsername
        setupUsername();
        final ListView listView = getListView();
        // Tell our list adapter that we only want 50 messages at a time
        mForumListAdapter = new ForumListAdapter(mFirebaseRef, getActivity(), R.layout.forummessage,
                mUsername);
        listView.setAdapter(mForumListAdapter);
        mForumListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(mForumListAdapter.getCount() - 1);
            }
        });
    }

    private void setupUsername() {
        mUsername = "Yoonho";
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button postForum = (Button) getActivity().findViewById(R.id.postButton);
        postForum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PostForumActivity.class);
                //intent.putExtra("objectType", "public");
                getActivity().startActivity(intent);
            }
        });

    }
}
