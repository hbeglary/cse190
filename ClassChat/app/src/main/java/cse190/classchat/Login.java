package cse190.classchat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

public class Login extends AppCompatActivity {

    private EditText email;
    private EditText passward;
    private AlertDialog.Builder alertDialog;
    private static final String FIREBASE_URL = "https://cse190classchat.firebaseio.com/";
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Firebase.setAndroidContext(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void LoginClick(View view){
        Firebase ref = new Firebase(FIREBASE_URL);
        String email = ((EditText) findViewById(R.id.email_text)).getText().toString();
        String password = ((EditText) findViewById(R.id.password_text)).getText().toString();


        ref.authWithPassword(email,password, new Firebase.AuthResultHandler(){

            @Override
            public void onAuthenticated(AuthData authData) {
                startActivity(new Intent(context, MainActivity.class));
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Alert(firebaseError.getMessage());
            }
        });

    }

    public void RegisterClick(View view){
        startActivity(new Intent(this, Register.class));
    }

    private void Alert(String msg){
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(msg);

        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = alertDialog.create();
        alert11.show();

    }

    public void ForgotPassword(View view){
        startActivity(new Intent(this, ResetPassword.class));
    }
}

